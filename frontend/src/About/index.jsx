import React, { useState, useEffect, useRef } from "react"
import imgNv from "../img/hd.png"
import { Card } from "react-bootstrap"
import "../style.css"

const About = () => {

    return (
        <>

            <div>
                <nav class="navbar navbar-expand-lg navbar-light div-header" >
                    <div class="container-fluid container-header" >
                        <a class="navbar-brand" href="/home" style={{ display: "contents" }} >
                            <img className="img-header" src={imgNv} ></img>
                            <h1 className="font-header" style={{ marginLeft: "10px", textAlign: "center", color: "#433c54", fontFamily: "lucida handwriting, cursive", marginTop: "1.5rem", marginBottom: "-3px", marginRight: "20px", fontWeight: "bolder" }}>Aromanja Coffee</h1>
                        </a>
                        <button class="navbar-toggler" style={{ marginBottom: "-20px" }} type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarNavDropdown" style={{ marginBottom: "-20px" }}>
                            <ul class="navbar-nav ms-auto">
                                <li class="nav-item">
                                    <a class="nav-link  nav-not-active font-nav" aria-current="page" href="/home" style={{ fontSize: "25px", fontFamily: "Titillium Web" }}>Home</a>
                                </li>
                                <li class="nav-item ">
                                    <a class="nav-link nav-not-active font-nav" href="/menu" style={{ fontSize: "25px", fontFamily: "Titillium Web" }}>Menu</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link nav-not-active font-nav" href="/franchising" style={{ fontSize: "25px", fontFamily: "Titillium Web", color: "#bf8226" }} >Franchising</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link nav-active font-nav" href="/about" style={{ fontSize: "25px", fontFamily: "Titillium Web", color: "#bf8226" }} >About </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>

                <hr className="line-header"></hr>

                <div className="breadcrumb-1">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a className="font-breadcrumb-active font-nav" href="/">Home</a></li>
                            <li class="breadcrumb-item font-breadcrumb-not-active font-nav" aria-current="page">About</li>
                        </ol>
                    </nav>
                </div>

                <div class="ratio ratio-16x9">
                    <iframe src="https://www.youtube.com/embed/Ew4X2Ogk55k?rel=0" title="YouTube video" allowfullscreen></iframe>
                </div>

                <div style={{ marginTop: "20px" }}>
                    <Card style={{ width: '100%', borderColor: "darkgray", borderStyle: "dashed" }}>
                        <Card.Body style={{ textAlign: "center" }}>
                            <p className="card-title">AROMANJA COFFEE</p>
                            <Card.Text style={{ textAlign: "justify" }}>
                                <p>
                                    Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quis ut fugiat consequatur illo dignissimos ducimus fugit assumenda ipsum quos, blanditiis cum vero, ab nemo officia, commodi ea delectus asperiores nulla.
                                    Lorem ipsum dolor, sit amet consectetur adipisicing elit. Sint atque maiores, voluptatem, veniam alias adipisci quibusdam ea libero est optio sit. Quis quidem possimus, in labore quos nobis nihil iure!
                                </p>

                                <p>
                                    Lorem ipsum dolor sit amet consectetur, adipisicing elit. Eveniet mollitia quod excepturi tenetur, odit sint perspiciatis ducimus similique sed officiis consequuntur nobis quae qui quo dolorem cupiditate omnis temporibus dolorum.
                                    Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ab nesciunt dolorem exercitationem, sunt ad mollitia est quis nam tenetur deleniti, id libero hic impedit nostrum magni dolor, earum quos laboriosam.
                                </p>
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </div>

            </div>
        </>
    )
}

export default About
import React, { useState, useEffect, useRef } from "react"
import "../style.css"
import img1 from "../img/menu-noncoffee/noncoffe-1.jpeg"
import img2 from "../img/menu-noncoffee/noncoffe-2.jpeg"
import img3 from "../img/menu-noncoffee/noncoffe-3.jpeg"
import img4 from "../img/menu-noncoffee/noncoffe-4.jpeg"
import img5 from "../img/menu-noncoffee/noncoffe-5.jpeg"
import img6 from "../img/menu-noncoffee/noncoffe-6.jpeg"

const menuNonCoffe = () => {

    return (
        <>

            <div>

                <div class="row row-cols-1 row-cols-md-2 g-4">
                    <div class="col">
                        <div class="card">
                            <img src={img1} class="card-img-top" alt="non-coffee" />
                            <div class="card-body">
                                <p class="card-title font-card-title text-center">Hot Matcha</p>
                                {/* <p class="card-text font-card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p> */}
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card">
                            <img src={img2} class="card-img-top" alt="non-coffee" />
                            <div class="card-body">
                                <p class="card-title font-card-title text-center">Iced Chocolate (Bali Origin)</p>
                                {/* <p class="card-text font-card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p> */}
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card">
                            <img src={img3} class="card-img-top" alt="non-coffee" />
                            <div class="card-body">
                                <p class="card-title font-card-title text-center">Hot Red Velvet</p>
                                {/* <p class="card-text font-card-text">This is a longer card with supporting text below as a natural lead-in to additional content.</p> */}
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card">
                            <img src={img4} class="card-img-top" alt="non-coffee" />
                            <div class="card-body">
                                <p class="card-title font-card-title text-center">Iced Matcha</p>
                                {/* <p class="card-text font-card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p> */}
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card">
                            <img src={img5} class="card-img-top" alt="non-coffee" />
                            <div class="card-body">
                                <p class="card-title font-card-title text-center">Iced Red Velvet</p>
                                {/* <p class="card-text font-card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p> */}
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card">
                            <img src={img6} class="card-img-top" alt="non-coffee" />
                            <div class="card-body">
                                <p class="card-title font-card-title text-center">Iced Taro Latte</p>
                                {/* <p class="card-text font-card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p> */}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default menuNonCoffe
import React, { useState, useEffect, useRef } from "react"
import imgNv from "../img/hd.png"
import { Tab } from "bootstrap"
import { Tabs } from "react-bootstrap"
import MenuCoffe from "./menuCoffe"
import MenuTea from "./menuTea"
import MenuNonCoffe from "./menuNonCoffe"
import MenuBottle from "./menuBottled"
import MenuBites from "./menuBites"
import img1 from "../img/menu-coffee/coffe-1.jpeg"
import img2 from "../img/menu-tea/tea-1.jpeg"
import img3 from "../img/menu-noncoffee/noncoffe-1.jpeg"
import img4 from "../img/menu-bottled/bottle-2.JPG"
import "./style.css"
import "../style.css"
import menuNonCoffe from "./menuNonCoffe"

const Menu = () => {


    return (
        <>

            <div>
                <nav class="navbar navbar-expand-lg navbar-light div-header" >
                    <div class="container-fluid container-header" >
                        <a class="navbar-brand" href="/home" style={{ display: "contents" }} >
                            <img className="img-header" src={imgNv} ></img>
                            <h1 className="font-header" style={{ marginLeft: "10px", textAlign: "center", color: "#433c54", fontFamily: "lucida handwriting, cursive", marginTop: "1.5rem", marginBottom: "-3px", marginRight: "20px", fontWeight: "bolder" }}>Aromanja Coffee</h1>
                        </a>
                        <button class="navbar-toggler" style={{marginBottom:"-20px"}} type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarNavDropdown" style={{ marginBottom: "-20px" }}>
                            <ul class="navbar-nav ms-auto">
                                <li class="nav-item">
                                    <a class="nav-link  nav-not-active font-nav" aria-current="page" href="/home" style={{ fontSize: "25px", fontFamily: "Titillium Web" }}>Home</a>
                                </li>
                                <li class="nav-item ">
                                    <a class="nav-link nav-active font-nav" href="/menu" style={{ fontSize: "25px", fontFamily: "Titillium Web" }}>Menu</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link nav-not-active font-nav" href="/franchising" style={{ fontSize: "25px", fontFamily: "Titillium Web", color: "#bf8226" }} >Franchising</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link nav-not-active font-nav" href="/about" style={{ fontSize: "25px", fontFamily: "Titillium Web", color: "#bf8226" }} >About </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>

                <hr className="line-header"></hr>

                <div className="filter-style breadcrumb-1">

                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a className="font-breadcrumb-active font-nav" href="/">Home</a></li>
                            <li class="breadcrumb-item font-breadcrumb-not-active font-nav" aria-current="page">Menu</li>
                        </ol>
                    </nav>


                    {/* <select class="form-select select-menu" aria-label="Default select example" >
                        <option selected hidden>Pilih Menu</option>
                        <option value="1">Coffee</option>
                        <option value="2">Tea</option>
                        <option value="3">Drink Non-Coffe</option>
                        <option value="4">Bottled Beverages</option>
                        <option value="5">Bites by Aromanja</option>
                    </select> */}

                </div>



                <div id="carouselExampleDark" class="carousel carousel-dark slide" data-bs-ride="carousel" >

                    <div class="carousel-indicators">
                        <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                        <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="1" aria-label="Slide 2"></button>
                        <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="2" aria-label="Slide 3"></button>
                        <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="3" aria-label="Slide 4"></button>
                        {/* <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="4" aria-label="Slide 5"></button> */}
                    </div>


                    <div class="carousel-inner">
                        <div class="carousel-item active" data-bs-interval="2000">
                            <img src={img1} class="d-block w-100" alt="..." />
                            <div class="carousel-caption d-none d-md-block">
                                <h2 style={{ color: "black", textAlign: "left", marginLeft: "-10%" }}>Coffee</h2>
                                {/* <h4 style={{ color: "#bf8226", textAlign: "left", marginLeft: "-10%" }}>Some representative placeholder content for the first slide.</h4> */}
                            </div>
                        </div>
                        <div class="carousel-item" data-bs-interval="2000">
                            <img src={img2} class="d-block w-100" alt="..." />
                            <div class="carousel-caption d-none d-md-block">
                                <h2 style={{ color: "black", textAlign: "left", marginLeft: "-10%" }}>Tea</h2>
                                {/* <h4 style={{ color: "#bf8226", textAlign: "left", marginLeft: "-10%" }}>Some representative placeholder content for the second slide.</h4> */}
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src={img3} class="d-block w-100" alt="..." />
                            <div class="carousel-caption d-none d-md-block">
                                <h2 style={{ color: "black", textAlign: "left", marginLeft: "-10%" }}>Drink Non-Coffee</h2>
                                {/* <h4 style={{ color: "#bf8226", textAlign: "left", marginLeft: "-10%" }}>Some representative placeholder content for the third slide.</h4> */}
                            </div>
                        </div>
                        <div class="carousel-item" data-bs-interval="2000">
                            <img src={img4} class="d-block w-100" alt="..." />
                            <div class="carousel-caption d-none d-md-block">
                                <h2 style={{ color: "black", textAlign: "left", marginLeft: "-10%" }}>Bottled Beverages</h2>
                                {/* <h4 style={{ color: "#bf8226", textAlign: "left", marginLeft: "-10%" }}>Some representative placeholder content for the second slide.</h4> */}
                            </div>
                        </div>
                        {/* <div class="carousel-item" data-bs-interval="2000">
                            <img src={img5} class="d-block w-100" alt="..." />
                            <div class="carousel-caption d-none d-md-block">
                                <h2 style={{ color: "black", textAlign: "left", marginLeft: "-10%" }}>Bites by Armonaja</h2>
                                <h4 style={{ color: "#bf8226", textAlign: "left", marginLeft: "-10%" }}>Some representative placeholder content for the second slide.</h4>
                            </div>
                        </div> */}
                    </div>

                    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleDark" data-bs-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Previous</span>
                    </button>
                    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleDark" data-bs-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Next</span>
                    </button>
                </div>

                <div style={{ marginTop: "20px", textAlign: "center" }}>
                    <p className="font-body-1">
                        What’s your mood? We’ve got exceptionally premium sourced coffee, and other crave-inducing treats prepared just for you.
                    </p>
                </div>

                <div className="card">


                    <Tabs defaultActiveKey="Coffee" id="uncontrolled-tab-example" className="mb-3 font-body-menu" style={{ justifyContent: "center", backgroundColor: "lavenderblush" }} >
                        <Tab eventKey="Coffee" title="Coffee">
                            <p>
                                <MenuCoffe />
                            </p>
                        </Tab>
                        <Tab eventKey="Tea" title="Tea">
                            <p>
                                <MenuTea />
                            </p>
                        </Tab>
                        <Tab eventKey="Drink Non Coffee" title="Drink Non Coffee">
                            <p>
                                <MenuNonCoffe />
                            </p>
                        </Tab>
                        <Tab eventKey="Bottled Beverages" title="Bottled Beverages">
                            <p>
                                <MenuBottle />
                            </p>
                        </Tab>
                        {/* <Tab eventKey="Bites by Aromanja" title="Bites by Aromanja">
                            <p>
                                <MenuBites />
                            </p>
                        </Tab> */}
                    </Tabs>

                </div>


            </div>
        </>
    )
}

export default Menu
import React, { useState, useEffect, useRef } from "react"
import "../style.css"
import img1 from "../img/menu-bites/bites-1.jpg"
import img2 from "../img/menu-bites/bites-2.jpg"
import img3 from "../img/menu-bites/bites-3.jpg"
import img4 from "../img/menu-bites/bites-4.jpg"
import img5 from "../img/home/home-5.jpeg"
import img6 from "../img/home/home-6.jpeg"
import img7 from "../img/home/home-7.jpeg"
import img8 from "../img/home/home-8.jpeg"
import img9 from "../img/home/home-9.jpeg"
import img10 from "../img/home/home-10.jpeg"

const menuBites = () => {

    return (
        <>

            <div>
                
                <div class="row row-cols-1 row-cols-md-2 g-4">
                    <div class="col">
                        <div class="card">
                            <img src={img1} class="card-img-top" alt="..." />
                            <div class="card-body">
                                <p class="card-title font-card-title">Card title</p>
                                <p class="card-text font-card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card">
                            <img src={img2} class="card-img-top" alt="..." />
                            <div class="card-body">
                                <p class="card-title font-card-title">Card title</p>
                                <p class="card-text font-card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card">
                            <img src={img3} class="card-img-top" alt="..." />
                            <div class="card-body">
                                <p class="card-title font-card-title">Card title</p>
                                <p class="card-text font-card-text">This is a longer card with supporting text below as a natural lead-in to additional content.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card">
                            <img src={img4} class="card-img-top" alt="..." />
                            <div class="card-body">
                                <p class="card-title font-card-title">Card title</p>
                                <p class="card-text font-card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </>
    )
}

export default menuBites
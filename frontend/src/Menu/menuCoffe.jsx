import React, { useState, useEffect, useRef } from "react"
import "../style.css"
import img1 from "../img/menu-coffee/coffe-1.jpeg"
import img2 from "../img/menu-coffee/coffe-2.jpeg"
import img3 from "../img/menu-coffee/coffe-3.jpeg"
import img4 from "../img/menu-coffee/coffe-4.jpeg"
import img5 from "../img/menu-coffee/coffe-5.jpeg"
import img6 from "../img/menu-coffee/coffe-6.jpeg"
import img7 from "../img/menu-coffee/coffe-7.jpeg"
import img8 from "../img/menu-coffee/coffe-8.jpeg"
import img9 from "../img/menu-coffee/coffe-9.jpeg"
import img10 from "../img/menu-coffee/coffe-10.jpeg"
import img11 from "../img/menu-coffee/coffe-11.jpeg"
import img12 from "../img/menu-coffee/coffe-12.jpeg"
import img13 from "../img/menu-coffee/coffe-13.jpeg"
import img14 from "../img/menu-coffee/coffe-14.jpeg"

const menuCoffe = () => {

    return (
        <>

            <div>
                
            <div class="row row-cols-1 row-cols-md-2 g-4">
                    <div class="col">
                        <div class="card">
                            <img src={img1} class="card-img-top" alt="coffe" />
                            <div class="card-body">
                                <p class="card-title font-card-title text-center">Cafe Latte</p>
                                {/* <p class="card-text font-card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p> */}
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card">
                            <img src={img2} class="card-img-top" alt="coffe" />
                            <div class="card-body">
                                <p class="card-title font-card-title text-center">Es Kopi Seram</p>
                                {/* <p class="card-text font-card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p> */}
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card">
                            <img src={img3} class="card-img-top" alt="coffe" />
                            <div class="card-body">
                                <p class="card-title font-card-title text-center">Espresso</p>
                                {/* <p class="card-text font-card-text">This is a longer card with supporting text below as a natural lead-in to additional content.</p> */}
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card">
                            <img src={img4} class="card-img-top" alt="coffe" />
                            <div class="card-body">
                                <p class="card-title font-card-title text-center">Es Kopi Sendu</p>
                                {/* <p class="card-text font-card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p> */}
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card">
                            <img src={img5} class="card-img-top" alt="coffe" />
                            <div class="card-body">
                                <p class="card-title font-card-title text-center">Hot Mocha</p>
                                {/* <p class="card-text font-card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p> */}
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card">
                            <img src={img6} class="card-img-top" alt="coffe" />
                            <div class="card-body">
                                <p class="card-title font-card-title text-center">Hot Americano</p>
                                {/* <p class="card-text font-card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p> */}
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card">
                            <img src={img7} class="card-img-top" alt="coffe" />
                            <div class="card-body">
                                <p class="card-title font-card-title text-center">Iced Cappucino</p>
                                {/* <p class="card-text font-card-text">This is a longer card with supporting text below as a natural lead-in to additional content.</p> */}
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card">
                            <img src={img8} class="card-img-top" alt="coffe" />
                            <div class="card-body">
                                <p class="card-title font-card-title text-center">Iced Latte</p>
                                {/* <p class="card-text font-card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p> */}
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card">
                            <img src={img9} class="card-img-top" alt="coffe" />
                            <div class="card-body">
                                <p class="card-title font-card-title text-center">Insomniac</p>
                                {/* <p class="card-text font-card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p> */}
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card">
                            <img src={img10} class="card-img-top" alt="coffe" />
                            <div class="card-body">
                                <p class="card-title font-card-title text-center">Pink Lady</p>
                                {/* <p class="card-text font-card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p> */}
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card">
                            <img src={img11} class="card-img-top" alt="coffe" />
                            <div class="card-body">
                                <p class="card-title font-card-title text-center">Single Origin (Japanase)</p>
                                {/* <p class="card-text font-card-text">This is a longer card with supporting text below as a natural lead-in to additional content.</p> */}
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card">
                            <img src={img12} class="card-img-top" alt="coffe" />
                            <div class="card-body">
                                <p class="card-title font-card-title text-center">Single Origin (V60)</p>
                                {/* <p class="card-text font-card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p> */}
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card">
                            <img src={img13} class="card-img-top" alt="coffe" />
                            <div class="card-body">
                                <p class="card-title font-card-title text-center">Single Origin (Kopi Tubruk)</p>
                                {/* <p class="card-text font-card-text">This is a longer card with supporting text below as a natural lead-in to additional content.</p> */}
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card">
                            <img src={img14} class="card-img-top" alt="coffe" />
                            <div class="card-body">
                                <p class="card-title font-card-title text-center">Vietnamesse Coffee Drip</p>
                                {/* <p class="card-text font-card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p> */}
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </>
    )
}

export default menuCoffe
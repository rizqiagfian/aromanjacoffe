import React, { useState, useEffect, useRef } from "react"
import "../style.css"
import img1 from "../img/menu-tea/tea-1.jpeg"
import img2 from "../img/menu-tea/tea-2.jpeg"
import img3 from "../img/menu-tea/tea-3.jpeg"
import img4 from "../img/menu-tea/tea-4.jpeg"
import img5 from "../img/menu-tea/tea-5.jpeg"


const menuTea = () => {

    return (
        <>

            <div>

                <div class="row row-cols-1 row-cols-md-2 g-4">
                    <div class="col">
                        <div class="card">
                            <img src={img1} class="card-img-top" alt="tea" />
                            <div class="card-body">
                                <p class="card-title font-card-title text-center">Milk Tea</p>
                                {/* <p class="card-text font-card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p> */}
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card">
                            <img src={img2} class="card-img-top" alt="tea" />
                            <div class="card-body">
                                <p class="card-title font-card-title text-center">Iced Lemon Tea</p>
                                {/* <p class="card-text font-card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p> */}
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card">
                            <img src={img3} class="card-img-top" alt="tea" />
                            <div class="card-body">
                                <p class="card-title font-card-title text-center">Iced Lychee Tea</p>
                                {/* <p class="card-text font-card-text">This is a longer card with supporting text below as a natural lead-in to additional content.</p> */}
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card">
                            <img src={img4} class="card-img-top" alt="tea" />
                            <div class="card-body">
                                <p class="card-title font-card-title text-center">Iced Shaken Tea</p>
                                {/* <p class="card-text font-card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p> */}
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card">
                            <img src={img5} class="card-img-top" alt="tea" />
                            <div class="card-body">
                                <p class="card-title font-card-title text-center">Iced Peach Tea</p>
                                {/* <p class="card-text font-card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p> */}
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </>
    )
}

export default menuTea
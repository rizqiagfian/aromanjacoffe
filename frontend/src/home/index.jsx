import React, { useState, useEffect, useRef } from "react"
import { Card } from "react-bootstrap"
import imgNv from "../img/hd.png"
import img1 from "../img/home/home-1.jpeg"
import img2 from "../img/home/home-2.jpeg"
import img3 from "../img/home/home-3.jpeg"
import img4 from "../img/home/home-4.jpeg"
import img5 from "../img/home/home-5.jpeg"
import img6 from "../img/home/home-6.jpeg"
import img7 from "../img/home/home-7.jpeg"
import img8 from "../img/home/home-8.jpeg"
import img9 from "../img/home/home-9.jpeg"
import img10 from "../img/home/home-10.jpeg"
import outlet from "../img/home/outlet.jpeg"
import ourmenu from "../img/home/ourmenu.jpg"
import "../style.css"
import "./style.css"

const Home = () => {

    return (
        <>

            <div>

                <nav class="navbar navbar-expand-lg navbar-light div-header" >
                    <div class="container-fluid container-header">
                        <a class="navbar-brand" href="/home" style={{ display: "contents" }} >
                            <img className="img-header" src={imgNv} ></img>
                            <h1 className="font-header" style={{ marginLeft: "10px", textAlign: "center", color: "#433c54", fontFamily: "lucida handwriting, cursive", marginTop: "1.5rem", marginBottom: "-3px", marginRight: "20px", fontWeight: "bolder" }}>Aromanja Coffee</h1>
                        </a>
                        <button class="navbar-toggler" style={{marginBottom:"-20px"}} type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarNavDropdown" style={{ marginBottom: "-20px" }}>
                            <ul class="navbar-nav ms-auto">
                                <li class="nav-item">
                                    <a class="nav-link  nav-active font-nav" aria-current="page" href="/home" style={{ fontSize: "25px", fontFamily: "Titillium Web" }}>Home</a>
                                </li>
                                <li class="nav-item ">
                                    <a class="nav-link nav-not-active font-nav" href="/menu" style={{ fontSize: "25px", fontFamily: "Titillium Web" }}>Menu</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link nav-not-active font-nav" href="/franchising" style={{ fontSize: "25px", fontFamily: "Titillium Web", color: "#bf8226" }} >Franchising</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link nav-not-active font-nav" href="/about" style={{ fontSize: "25px", fontFamily: "Titillium Web", color: "#bf8226" }} >About </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>

                <hr className="line-header"></hr>

                <div id="carouselExampleDark" class="carousel carousel-dark slide" data-bs-ride="carousel" >


                    <div class="carousel-indicators">
                        <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                        <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="1" aria-label="Slide 2"></button>
                        <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="2" aria-label="Slide 3"></button>
                        <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="3" aria-label="Slide 4"></button>
                        <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="4" aria-label="Slide 5"></button>
                        <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="5" aria-label="Slide 6"></button>
                        <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="6" aria-label="Slide 7"></button>
                        <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="7" aria-label="Slide 8"></button>
                        <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="8" aria-label="Slide 9"></button>
                        <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="9" aria-label="Slide 10"></button>
                    </div>


                    <div class="carousel-inner">
                        <div class="carousel-item active" data-bs-interval="2000">
                            <img src={img1} class="d-block w-100" alt="..." />
                            {/* <div class="carousel-caption d-none d-md-block">
                                <h2 style={{ color: "black", textAlign: "left", marginLeft: "-10%" }}>Chocolate</h2>
                                <h4 style={{ color: "#bf8226", textAlign: "left", marginLeft: "-10%" }}>Some representative placeholder content for the first slide.</h4>
                            </div> */}
                        </div>
                        <div class="carousel-item" data-bs-interval="2000">
                            <img src={img2} class="d-block w-100" alt="..." />
                            {/* <div class="carousel-caption d-none d-md-block">
                                <h2 style={{ color: "black", textAlign: "left", marginLeft: "-10%" }}>Americano</h2>
                                <h4 style={{ color: "#bf8226", textAlign: "left", marginLeft: "-10%" }}>Some representative placeholder content for the second slide.</h4>
                            </div> */}
                        </div>
                        <div class="carousel-item">
                            <img src={img3} class="d-block w-100" alt="..." />
                            {/* <div class="carousel-caption d-none d-md-block">
                                <h2 style={{ color: "black", textAlign: "left", marginLeft: "-10%" }}>Espresso</h2>
                                <h4 style={{ color: "#bf8226", textAlign: "left", marginLeft: "-10%" }}>Some representative placeholder content for the third slide.</h4>
                            </div> */}
                        </div>
                        <div class="carousel-item" data-bs-interval="2000">
                            <img src={img4} class="d-block w-100" alt="..." />
                            {/* <div class="carousel-caption d-none d-md-block">
                                <h2 style={{ color: "black", textAlign: "left", marginLeft: "-10%" }}>Americano</h2>
                                <h4 style={{ color: "#bf8226", textAlign: "left", marginLeft: "-10%" }}>Some representative placeholder content for the second slide.</h4>
                            </div> */}
                        </div>
                        <div class="carousel-item">
                            <img src={img5} class="d-block w-100" alt="..." />
                            {/* <div class="carousel-caption d-none d-md-block">
                                <h2 style={{ color: "black", textAlign: "left", marginLeft: "-10%" }}>Espresso</h2>
                                <h4 style={{ color: "#bf8226", textAlign: "left", marginLeft: "-10%" }}>Some representative placeholder content for the third slide.</h4>
                            </div> */}
                        </div>
                        <div class="carousel-item" data-bs-interval="2000">
                            <img src={img6} class="d-block w-100" alt="..." />
                            {/* <div class="carousel-caption d-none d-md-block">
                                <h2 style={{ color: "black", textAlign: "left", marginLeft: "-10%" }}>Americano</h2>
                                <h4 style={{ color: "#bf8226", textAlign: "left", marginLeft: "-10%" }}>Some representative placeholder content for the second slide.</h4>
                            </div> */}
                        </div>
                        <div class="carousel-item">
                            <img src={img7} class="d-block w-100" alt="..." />
                            {/* <div class="carousel-caption d-none d-md-block">
                                <h2 style={{ color: "black", textAlign: "left", marginLeft: "-10%" }}>Espresso</h2>
                                <h4 style={{ color: "#bf8226", textAlign: "left", marginLeft: "-10%" }}>Some representative placeholder content for the third slide.</h4>
                            </div> */}
                        </div>
                        <div class="carousel-item" data-bs-interval="2000">
                            <img src={img8} class="d-block w-100" alt="..." />
                            {/* <div class="carousel-caption d-none d-md-block">
                                <h2 style={{ color: "black", textAlign: "left", marginLeft: "-10%" }}>Americano</h2>
                                <h4 style={{ color: "#bf8226", textAlign: "left", marginLeft: "-10%" }}>Some representative placeholder content for the second slide.</h4>
                            </div> */}
                        </div>
                        <div class="carousel-item">
                            <img src={img9} class="d-block w-100" alt="..." />
                            {/* <div class="carousel-caption d-none d-md-block">
                                <h2 style={{ color: "black", textAlign: "left", marginLeft: "-10%" }}>Espresso</h2>
                                <h4 style={{ color: "#bf8226", textAlign: "left", marginLeft: "-10%" }}>Some representative placeholder content for the third slide.</h4>
                            </div> */}
                        </div>
                        <div class="carousel-item" data-bs-interval="2000">
                            <img src={img10} class="d-block w-100" alt="..." />
                            {/* <div class="carousel-caption d-none d-md-block">
                                <h2 style={{ color: "black", textAlign: "left", marginLeft: "-10%" }}>Americano</h2>
                                <h4 style={{ color: "#bf8226", textAlign: "left", marginLeft: "-10%" }}>Some representative placeholder content for the second slide.</h4>
                            </div> */}
                        </div>

                    </div>
                    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleDark" data-bs-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Previous</span>
                    </button>
                    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleDark" data-bs-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Next</span>
                    </button>
                </div>

                <br />

                <div>
                    <Card style={{ width: '100%', borderColor: "darkgray", borderStyle: "dashed" }}>
                        <Card.Body style={{ textAlign: "center" }}>
                            <p className="card-title">WELCOME TO AROMANJA COFFEE !</p>
                            <Card.Text>
                                Lorem ipsum dolor sit, amet consectetur adipisicing elit. Eos illo aut est culpa sed reprehenderit nobis deleniti eius architecto modi. Facilis dicta aspernatur reprehenderit iste porro laborum sapiente fugiat vitae.
                                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </div>

                <div>
                    <div class="card-group" style={{ marginTop: "20px" }}>
                        <div class="card card-home" style={{ padding: "1rem", border: "none", textAlign:"center" }}>
                            <img src={ourmenu} class="d-block w-100" alt="..." />
                            <p className="font-home-1"  style={{marginTop:"10px"}}> <a href="/menu" style={{ textDecoration: "underline", color: "chocolate"}}>
                                Explore Our Menu
                            </a> </p>
                        </div>

                        <div class="card card-home" style={{ padding: "1rem", border: "none",textAlign:"center" }}>
                            <img src={outlet} class="d-block w-100" alt="..." />
                            <p className="font-home-1" style={{marginTop:"10px"}}> <a href="/franchising" style={{ textDecoration: "underline", color: "chocolate" }}>
                                Be Our Partner
                            </a> </p>
                        </div>

                    </div>
                </div>

            </div>
        </>
    )
}

export default Home
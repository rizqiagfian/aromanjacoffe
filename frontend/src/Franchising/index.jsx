import React, { useState, useEffect, useRef } from "react"
import { Card } from "react-bootstrap"
import imgNv from "../img/hd.png"
import { Table } from "react-bootstrap"
import "../style.css"
import outlet from "../img/home/outlet.jpeg"
import outlet2 from "../img/home/outlet-2.jpeg"
import { Button } from "react-bootstrap"

const Franchising = () => {

    const [dataEntry] = useState([
        { header: 'License', value: ": Aromanja Brand License" }, { header: 'Espresso Machine', value: ": Semi-Commercial Ferratti Ferro espresso machine" },
        { header: 'Grinder', value: ": Built-in coffee grinder" },
        { header: 'Cold Storage', value: ": Showcase, freezer 200L" }, { header: 'Tablet', value: ": Samsung" }, { header: 'POS', value: ": MokaPOS" },
        { header: 'Promotion Media', value: ": Menu, Banner Design, Flyer" }, { header: 'Barista Apron', value: ": Included" },
        { header: 'Small Product Equipment', value: ": Sufficient" }, { header: 'Ingredients Quantitiy', value: ": 2 - 3 Weeks" },
        { header: 'Professional Training', value: ": 1 Weeks" },
        { header: 'Booth Design', value: ": Standart Booth Design" }
    ])

    const [dataModerate] = useState([
        { header: 'License', value: ": Aromanja Brand License" }, { header: 'Espresso Machine', value: ": Commercial Ferratti Ferro espresso machine" },
        { header: 'Grinder', value: ": Espresso + Manual Brew Grender" },
        { header: 'Cold Storage', value: ": Showcase, freezer 300L" }, { header: 'Tablet', value: ": Samsung" }, { header: 'POS', value: ": MokaPOS" },
        { header: 'Promotion Media', value: ": Menu, Banner Design, Flyer" }, { header: 'Barista Apron', value: ": Included" },
        { header: 'Small Product Equipment', value: ": High-grade" }, { header: 'Ingredients Quantitiy', value: ": 2 Month" },
        { header: 'Professional Training', value: ": 1 Weeks" },
        { header: 'Booth Design', value: ": Standart Booth Design" }
    ])

    const buttonOnclick = () => {
        window.open("https://drive.google.com/drive/u/0/folders/1HMqVFgkLqrmpdJUotQ31H4Zzx7p5Pl4X", "_blank")
    }

    return (
        <>
            <div>
                <div>

                    <nav class="navbar navbar-expand-lg navbar-light div-header">
                        <div class="container-fluid container-header">
                            <a class="navbar-brand" href="/home" style={{ display: "contents" }} >
                                <img className="img-header" src={imgNv} ></img>
                                <h1 className="font-header" style={{ marginLeft: "10px", textAlign: "center", color: "#433c54", fontFamily: "lucida handwriting, cursive", marginTop: "1.5rem", marginBottom: "-3px", marginRight: "20px", fontWeight: "bolder" }}>Aromanja Coffee</h1>
                            </a>
                            <button class="navbar-toggler" style={{ marginBottom: "-20px" }} type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                            <div class="collapse navbar-collapse" id="navbarNavDropdown" style={{ marginBottom: "-20px" }}>
                                <ul class="navbar-nav ms-auto">
                                    <li class="nav-item">
                                        <a class="nav-link  nav-not-active font-nav" aria-current="page" href="/home" style={{ fontSize: "25px", fontFamily: "Titillium Web" }}>Home</a>
                                    </li>
                                    <li class="nav-item ">
                                        <a class="nav-link nav-not-active font-nav" href="/menu" style={{ fontSize: "25px", fontFamily: "Titillium Web" }}>Menu</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link nav-active font-nav" href="/franchising" style={{ fontSize: "25px", fontFamily: "Titillium Web", color: "#bf8226" }} >Franchising</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link nav-not-active font-nav" href="/about" style={{ fontSize: "25px", fontFamily: "Titillium Web", color: "#bf8226" }} >About </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>

                    <hr className="line-header"></hr>

                </div>

                <div className="breadcrumb-1">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a className="font-breadcrumb-active font-nav" href="/">Home</a></li>
                            <li class="breadcrumb-item font-breadcrumb-not-active font-nav" aria-current="page">Franchising</li>
                        </ol>
                    </nav>
                </div>

                <div style={{ marginTop: "20px" }}>
                    <Card style={{ width: '100%', border: "none" }}>
                        <Card.Body style={{ textAlign: "center" }}>
                            <p className="card-title">PAKET FRANCHISE AROMANJA COFFEE</p>
                            <Card.Text style={{ textAlign: "justify" }}>
                                <div>
                                    <div class="card-group" style={{ marginTop: "20px" }}>
                                        <div class="card card-home" style={{ padding: "1rem", border: "none", textAlign: "center" }}>
                                            <img src={outlet2} class="d-block w-100" alt="..." />
                                            <p className="font-home-1" style={{ marginTop: "10px", backgroundColor: "lavenderblush", borderRadius: "20px", borderStyle: "groove" }}>
                                                Paket Entry Level
                                            </p>

                                            <Table >
                                                <tbody style={{ border: "groove" }}>
                                                    {dataEntry.map((data) => {
                                                        return (
                                                            <tr>
                                                                <td style={{ textAlign: "left", color: "chocolate", border: "none" }}>
                                                                    {data.header}
                                                                </td>
                                                                <td style={{ border: "none", textAlign: "left" }}>
                                                                    {data.value}
                                                                </td>
                                                            </tr>
                                                        )
                                                    })}
                                                </tbody>
                                            </Table>

                                        </div>

                                        <div class="card card-home" style={{ padding: "1rem", border: "none", textAlign: "center" }}>
                                            <img src={outlet} class="d-block w-100" alt="..." />
                                            <p className="font-home-1" style={{ marginTop: "10px", backgroundColor: "lavenderblush", borderRadius: "20px", borderStyle: "groove" }}>
                                                Paket Moderate Level
                                            </p>
                                            <Table >
                                                <tbody style={{ border: "groove" }}>
                                                    {dataModerate.map((data) => {
                                                        return (
                                                            <tr>
                                                                <td style={{ textAlign: "left", color: "chocolate", border: "none" }}>
                                                                    {data.header}
                                                                </td>
                                                                <td style={{ border: "none", textAlign: "left" }}>
                                                                    {data.value}
                                                                </td>
                                                            </tr>
                                                        )
                                                    })}
                                                </tbody>
                                            </Table>
                                        </div>

                                    </div>
                                </div>
                            </Card.Text>
                        </Card.Body>
                    </Card>

                    <hr style={{ marginTop: "-1rem" }} />

                    <Card style={{ width: '100%', border: "none", marginTop: "-1rem" }}>
                        <Card.Body style={{}}>
                            <Card.Text style={{}}>
                                <div>
                                    <div class="card-group" style={{marginTop:"-15px"}} >
                                        <div class="card card-home" style={{ border: "none" }} >
                                            <p style={{ textAlign: "center", fontWeight: "bold" }}>Persyaratan Standar Outlet</p>
                                            <ul type="square">
                                                <li>
                                                    Space 4 x 3 (12 m2) minimum
                                                </li>
                                                <li>
                                                    Listrik 10A (2200 Watt ) 1 phase
                                                </li>
                                                <li>
                                                    Jalur air bersih dan air kotor
                                                </li>
                                            </ul>

                                        </div>

                                        <div class="card card-home" style={{ paddingTop: "2rem", border: "none", textAlign: "center" }}>
                                            <p style={{ fontWeight: "bold" }}>Download Katalog Franchise : </p>
                                            <div style={{ justifyContent: "center" }}>
                                                <Button variant="outline-success" onClick={buttonOnclick} style={{ borderRadius: "20px", width: "80%" }}>
                                                    Katalog Franchise Aromanja
                                                </Button>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </Card.Text>
                        </Card.Body>

                    </Card>

                </div>

            </div>
        </>
    )
}

export default Franchising